# blchrd.eu

This is the source file of my personal blog.

## Installing Hexo and Compiling file

    $ npm install hexo-cli -g
    $ hexo init blog
    $ cd blog
    $ npm install
    $ hexo serve
    
## Conversion of MarkDown source files into gemtext Gemini files

The script `generate-gmi-from-md` get all the markdown file in `source/_posts` and convert it into gemtext Gemini files.

If you want to use it, you may want to change some values in it.