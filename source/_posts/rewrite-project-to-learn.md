---
title: Rewrite your projects to learn new languages and new techs
date: 2023-12-19 16:19:57
tags:
---


In the developer field - and by extension, in tech field - continuously learning new stuff, keeping yourself sharp at what you do and just keeping yourself updated is highly recommended, if not just required.

In my morning routine - no, I'll not talk about sport here -, the first thing I do is making coffee, and then scroll at my RSS feeds. Most of the stuff are just news about the world, gaming and music. But in them, there also are a lot of tech news and dev blog post - and yes, HackerNews too. While reading it, I can deduct the current "à la mode" techs and languages. I almost always take the "Getting started" of a tech I didn't know, write an `Hello world` and / or a todo-list, and then decide if I want to dig further or not.

While chatting with a fellow developer - maybe he'll recognize himself -, we start talking about the fact I want to learn Golang. Some languages are more appealing to me, and Golang is one of them. I already did the "Getting started" and some tutorials, but what I lacked to really start learning, is a project idea.

When I want to learn a language, I want to have at least a small project idea, to write a piece of software I find useful, or at least fun to write. The todo-list is always my first stop, but after that, I want to make more.

The discussion going, I had an idea: maybe I can rewrite the PlaylistShare backend in Golang, to learn it? There is no real downside in rewriting a project. You'll learn the language, you already have documentation about what kind of feature you'll write (well, if you document your project correctly), and some constraints - like, do not break the frontend. At the end, ok I'll have two backend, but I'll have more knowledge about the behavior of the language and the syntax of it, I can even have some new ideas about the backend itself, or even a new project ieea..

And I started it, rewriting my PHP/Laravel backend in Golang. I have a kind-of working version right now, and I understand a little better some specifics of the language. It's not magical though, I can't say I'm productive with Go, but I understand some stuff, and reading Go code is much easier right now.

This kind of rewriting has been a revelation, and I recommend to all the people who wants to learn another language to test it, if you didn't have an existing project to do it, well, start to make a project then. I know that'll not work for everyone, but for me, it works quite well. 

Right now, I try to learn Rust with a similar process, rewriting my HNWGen project (originally written in Java).
