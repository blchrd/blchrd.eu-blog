---
title: About note taking
date: 2023-09-10 17:04:17
tags:
---


## Introduction (kind of)

I take a lot of notes in various formats and on different devices, so it can be a bit complex to retrieve a note I wrote some time ago.

I used [CherryTree](https://www.giuspen.net/cherrytree/) a lot when I was in France, and it covered most of my needs. But after moving to Canada, I started to take notes on my phone and also began using the terminal frequently. Consequently, CherryTree was no longer as useful, or at least not the best tool for my current needs.

I should know how to list my needs because it is part of my job, and find or even create the software that covers all or most of those needs.

So I start doing it, and it was quite a journey.

## Why, when and how?

First, I need to take a step back in my note-taking process to understand why, when, and how I take notes.

Why? To avoid forgetting something, obviously. Sometimes it is just to help my memory (it helps me remember something, even if I don't reread it later). It can help me organize and prioritize tasks. Or it's just to throw distracting ideas out of my brain to focus on what I'm doing right now. I don't really have to remember or even retrieve the last one; it's really about removing distractions.

When? This one is easy: at any time, day or night.

How? Well, here is the tricky question. It is chaotic. I use a lot of support and different devices (pen and paper, Android phone, both Windows and Linux PCs). I don't organize it very well. It can be on a post-it with only a word or two, or it can be a markdown file hidden on my hard drive with a long reflection about a topic or an idea I'm trying to elaborate on. It can also be a full notebook on a specific software, like my CherryTree notebook.

The format of one note is almost always the same: a list of items. It is rare that I note a full paragraph, or it's a full webpage backup. Yes, I sometimes download a webpage to read it offline, or even just for backup purposes.

I'm leaving the "where" question aside because I don't find it relevant here; the answer is pretty much the same as "when": everywhere.

## Try to define my needs

Until now, I have been using CherryTree for taking my notes and syncing them with Dropbox. It has worked quite well up to this point. However, I now have a significant number of notes on my phone, and there isn't an Android version of CherryTree available. So, I'm faced with two options: either I develop an Android version myself or I change my workflow. Both options are time-consuming, but I've decided to go with the second one.

I need to narrow down my requirements in order to choose the right piece of software:

* I love taking notes on paper, I just need a pen and a physical notebook, not sure this point is relevant.
* I prefer Markdown syntax or a similar format because I find it more convenient. It will likely be easier to write a conversion script for my CherryTree notebooks.
* I require synchronization between multiple devices and operating systems (Android, Windows, Linux, etc.), I can consider manual synchronization.
* I need a tag system, even though I haven't used tags extensively in the past. I recognize the importance of using such a system to facilitate easy searching within my notes.
* I would like the software to have a command-line interface (CLI) application for adding, editing, and removing notes on both Windows and Linux.

I have one more constraint: I do not want to use a centralized note service like Evernote or Obsidian synchronization. I want the flexibility to store my notes wherever I choose and to move them to a different location if needed.

## Softwares and current workflow 

I read about [nb](https://xwmx.github.io/nb/) while browsing the [geminispace](https://geminiprotocol.net/), and it interested me immediately. It is a CLI / TUI notetaking app with many really cool features: bookmarks (with a local copy for offline reading), to-do list, tags, git synchronization, and more. Plus, it stores notes as simple markdown files on the hard drive, making it easy to synchronize other files and add them.

As for Android, I tried various applications. First, I explored some on the Play Store, but then I started developing my own. I've been using it for 4 or 5 months now. I was already familiar with [Markor](https://github.com/gsantner/markor), but I didn't fully utilize it until I embarked on a three-month journey in North America.

During my travels, I began taking extensive notes on my phone, and Markor proved to be a straightforward application: one file for one note. It also includes a to-do list feature, although I didn't make much use of it, it's worth mentioning.

Lastly, there's [Syncthing](https://syncthing.net/). This application stands out as one of the best discoveries I've made this year. It allows you to seamlessly synchronize content between your smartphone and your PC. It offers a plethora of settings when configuring the sync, and most importantly, it works exceptionally well and is easy to use.

All of these elements have converged into the workflow I've been experimenting with:

* nb: for PCs, both on Linux and Windows (with WSL for the latter, so it's essentially still Linux).
* Markor + Syncthing: for my Android phone.

I'm considering writing some automation scripts for syncing, even though it's quite usable without them. I know it will be fun to create these scripts.

Here are some issues:

* Syncing with Android and Windows: Currently, I use nb with WSL, and I use rsync in addition to Syncthing when I really need to synchronize notes directly between Android and Windows.
* Viewing notes on Android: I can read my notes on git directly with Android, but searching isn't really an option.

A couple of thoughts on Pen & Paper synchronization, just for fun:

* Take pictures of the notebook page and synchronize the photo with Syncthing/nb.
* Try to OCR the notebook page. I also gave it a try, but it didn't work well. However, I didn't really get into the technology.

## Conclusion (kind of)

I have been using this workflow for about a month now, and it has been working well so far. I highly recommend giving 'nb' a try if you're not afraid of the terminal.

It is just my train of thought on my note-taking system. I wanted to share this here, and if someone discovers one of the software I talk about here, I consider this post useful.

I still refer to my CherryTree notebook from time to time while working on the conversion script; for now, my notes are stuck in there. I've started working on a script for my personal use — it's advanced enough to [share the code](https://framagit.org/blchrd/ct2md), but it's far for completed — and I'll build upon it to create a full conversion script.

I've come to realize that I enjoy writing conversion scripts. It's not too complex, and it helps me become more comfortable with different formats and languages. I love these kinds of "coffee break" scripts and projects.

Take care, everyone!