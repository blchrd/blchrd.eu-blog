---
title: From Vanilla PHP to Hexo
date: 2023-07-27 10:10:32
tags:
---

I have had this website for less than a year, and I've been wondering what to do with it.

Until now, it has been just a small dynamic website where I put my work in progress and my most advanced projects, along with a little description of myself. I used a single PHP page that retrieves data from a bunch of .json files. Not gonna lie, I love developing this little thing.

But in the end, it wasn't enough. I want to do more with this website.

So I searched for a blog engine, and I have some requirements:

* Not a big blog engine; WordPress and others are not for me. I just want to share text and links, and using them would be overkill.
* I want to write blog posts and pages with Markdown, which is much easier than direct HTML editing or a WYSIWYG editor, at least for me.
* I want it to be easy to install and maintain, for obvious reasons.
* If it can generate a static website, that's even better, but it's not a strict requirement; I can do without this function.

After looking at some engines - like [Jekyll](https://jekyllrb.com/) and [Dropplets](https://github.com/johnroper100/dropplets), the two main contenders - I decided to give [Hexo](https://hexo.io/) a try. On paper, it checks all my points, and it has a lot of really cool templates too - I ended up using the [Cactus](https://github.com/probberechts/hexo-theme-cactus) theme here.

The installation is straightforward, five command lines in a terminal, and you're good to go.

```bash
npm install hexo-cli -g
hexo init blog
cd blog
npm install
hexo serve
```

Your blog is now running on `localhost:4000`, and a 'Hello World' page welcomes you, with all the instructions you need to create posts, generate the static files, and so on.

The configuration file is about a hundred lines, and the main items are easy to configure without the documentation (I wonder what `title`, in the `Site` section, will do...), but for the rest, the [documentation](https://hexo.io/docs/configuration) is really clear.

There is additional configuration for the theme you chose, even if you want to keep the original one.

After generating some test blog posts and testing some themes, I finally decided to go with Hexo and the cactus theme for my personal website. It checks all the requirements I had, and it is easy to understand, set up, configure, and maintain.

If you are wondering about which blog engine you want, and your requirements look like mine, I recommend you to give Hexo a try.