---
title: About me
date: 2024-05-31 11:42:16
---

# About me

*Learner, developer, sometimes musician.*

Hi there! I'm Thomas, a software and web developer with a passion for music and sound experimentation. I start to learn programming in 2007, and I never stopped since then, either learning or programming.

I have two main side projects:
- [PlaylistShare](https://plsh.blchrd.eu), a music tracking application I use daily, built using PHP Laravel and ReactJS. 
- An [experimental music album generator](https://framagit.org/blchrd/rust-hnwgen) for HNW ([Harsh Noise Wall](https://en.wikipedia.org/wiki/Harsh_noise_wall)) artists, originally written in Java, I rewrote it in Rust a while ago, and keep experimenting with procedural generation to improve it.

I thrive on learning new things daily, solving problems, and just improving myself overall.

Beyond the tech world, I'm an avid music and video game enthusiast, an average electric guitar player, and I also enjoy spending time hiking and walking. And coffee. Is it really worth living without coffee?